# Todo list Test Plan

############################################
- Project Name: Todo list exercise
- Created By: Terence Mlimi
- Creation date: 25-05-2021
#############################################


### Scope
- Todo list project - todo list is a list of things that one wants to get done, this project will allow users to add, edit, view, share and delete to do items

### Test Tool
- Selenium

### Test Type and objective
- Front-End Functional Test 
- Ensure that the implementation is working correctly as expected - no bugs.

### TEST CASE ID
- TC_001
- TC_002
- TC_003
- TC_004
- TC_005
- TC_006
- TC_007

### TEST SCENARIOS
- TC_001: Verify application can deploy in docker
- TC_002: Verify multiple users can view the shared public todo list on refresh
- TC_003: Verify todo list items persist after browser refresh
- TC_004: Verify todo list items are not empty
- TC_005: Verify adding to do items
- TC_006: Verify deleting todo items
- TC_007: Verify editing todo items

### PRE-CONDITION
- TC_001: Run Docker Image
- TC_002: Need multiple users must be online
- TC_003: Need chrome browser
- TC_004: Need a valid user
- TC_005: Need a valid user
- TC_006: Need a valid user
- TC_007: Need a valid user


### TEST STEPS
1. Create todo item
   - type name
   - click 'Submit' buttom

2. Edit todo item
   - select item ID from list
   - edit name
   - click 'Submit' buttom

3. View todo item
   - select item ID from list
   - view details

4. Delete todo item
   - select item ID
   - click 'Delete' buttom

### EXPECTED RESULTS
- TC_001: Application must be able to deploy in docker
- TC_002: Multiple users should be able to view the shared public todo list (no live updates, only on refresh)
- TC_003: Todo list items should persist after browser refresh
- TC_004: Todo items should not be able to be empty
- TC_005: Should be able to add todo items
- TC_006: Should be able to delete todo items
- TC_007: Should be able to edit todo items

### TEST DATA
- Valid input
- Invalid input


### API Test Actions
- Verify correct HTTP status code. For example adding todo list should return 201
- Verify response payload. Check valid JSON body and correct field names, types and values
- Verify response headers. Security and Perfomance
- Verify correct application state.
- Verify basic perfomance sanity. Check response time


### Exit Criteria
- Complete execution of all the test scenarios
