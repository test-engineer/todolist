# Todo list Bug Report

### Defect ID
- D_01 

### Defect Description
- todo list application failed to run

### Actual error
- Error: Cannot find module 'express'

Require stack:
- D:\Users\Terrence\Desktop\qe-todolist\app.js
    at Function.Module._resolveFilename (internal/modules/cjs/loader.js:885:15)
    at Function.Module._load (internal/modules/cjs/loader.js:730:27)
    at Module.require (internal/modules/cjs/loader.js:957:19)
    at require (internal/modules/cjs/helpers.js:88:18)
    at Object.<anonymous> (D:\Users\Terrence\Desktop\qe-todolist\app.js:1:15)
    at Module._compile (internal/modules/cjs/loader.js:1068:30)
    at Object.Module._extensions..js (internal/modules/cjs/loader.js:1097:10)
    at Module.load (internal/modules/cjs/loader.js:933:32)
    at Function.Module._load (internal/modules/cjs/loader.js:774:14)
    at Function.executeUserEntryPoint [as runMain] (internal/modules/run_main.js:72:12) {
  code: 'MODULE_NOT_FOUND',
  requireStack: [ 'D:\\Users\\Terrence\\Desktop\\qe-todolist\\app.js' ]
}

### Steps to reproduce
- Run `node app.js` in the project root folder


### Severity
- High

### State
- Opened


### Created By
- Terence Mlimi

### Date
- 25-05-2021

### Assigned to
- Developer 
